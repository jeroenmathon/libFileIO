//
// Created by jeroen on 9/15/15.
//

#ifndef BLACKBRUTE_FILEIO_H
#define BLACKBRUTE_FILEIO_H

#include <iostream>
#include <vector>
#include <fstream>
#include <memory>
#include <limits>
#include <algorithm>

class fileManager
{
public:
    // Constructors and Destructors
    fileManager();
    ~fileManager();

    // Public routines
    std::string getLine(int id, int line);  // Returns a line from a file
    int getLines(int id);                   // Returns the total amount of lines in a file
    void addFile(std::string path);          // Adds a file to the file register
    int removeFile(std::string path);       // Removes a file from the file register
    int writeData(int id, std::string data);// Writes strings to a file
    void clear();                           // Cleans up any remaining objects in memory
    void printFiles();

private:
    // Private routines
    int openFile(int id);                                             // Opens a file
    int closeFile(int id);                                            // Closes a file
    void reId();                                                      // Provides the files with updated id's
    std::fstream& GotoLine(std::fstream& file, unsigned int num);     // Seeks to a line in a file

    // Private variables
    class File
    {
    public:
        // Constructors and Destructors
        File(int _id, std::string _path)
        {
            id = _id;
            path = _path;
        }
        ~File(){};

        int id;
        std::string path;
        std::fstream file;
    };

    std::vector<std::unique_ptr<File>> files;
};
#endif //BLACKBRUTE_FILEIO_H
