# libFileIO
A c++ library to do simple text file IO

Documentation:



    std::string getLine(int id, int line);  // Returns a line from a file
    int getLines(int id);                   // Returns the total amount of lines in a file
    void addFile(std::string path);          // Adds a file to the file register
    int removeFile(std::string path);       // Removes a file from the file register
    int writeData(int id, std::string data);// Writes strings to a file
    void clear();                           // Cleans up any remaining objects in memory
    void printFiles();                      // Prints out the contents of the file register
