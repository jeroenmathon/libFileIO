//
// Created by jeroen on 9/15/15.
//

#include "fileio.h"

fileManager::fileManager()
{

}

fileManager::~fileManager()
{

}

std::string fileManager::getLine(int id, int line)
{
    std::string output;

    for(int i(0);i < files.size(); i++)
    {
        if(files[i]->id == id)
        {
            openFile(id);

            GotoLine(files[i]->file, line);
            std::getline(files[i]->file, output);

            closeFile(id);
        }
    }

    return output;
}

int fileManager::getLines(int id)
{
    for (int i(0); i < files.size(); i++)
    {
        if(files[i]->id == id)
        {
            openFile(id);

            int numLines = 0;

            std::string unused;
            while ( std::getline(files[i]->file, unused) )
                ++numLines;

            closeFile(id);

            return numLines;
        }
    }

    return 0;
}

void fileManager::addFile(std::string path)
{
    files.push_back(std::unique_ptr<File>(new File(files.size(), path)));
}

int fileManager::removeFile(std::string path)
{
    for(int i(0); i < files.size(); i++)
    {
        if(files[i]->path == path)
        {
            files.erase(files.begin()+i);
            reId();
            return 1;
        }
    }

    return 0;
}

int fileManager::writeData(int id, std::string data)
{
    for(int i(0); files.size(); i++)
    {
        if(files[i]->id == id)
        {
            openFile(id);

            files[i]->file << data;

            closeFile(id);

            return 1;
        }
    }

    return 0;
}

int fileManager::openFile(int id)
{
    for(int i(0);i < files.size(); i++)
    {
        if(files[i]->id == id)
        {
            files[i]->file.open(files[i]->path, std::fstream::in | std::fstream::out | std::fstream::app);
            return 1;
        }
    }

    return 0;
}

int fileManager::closeFile(int id)
{
    for(int i(0);i < files.size(); i++)
    {
        if(files[i]->id == id)
        {
            files[i]->file.close();
        }
    }
}

void fileManager::printFiles()
{
    for(int i(0); i < files.size(); i++)
    {
        std::cout << "ID:" << files[i]->id << " PATH:" << files[i]->path << "\n";
    }
}

void fileManager::clear()
{
    files.erase(files.begin(), files.end());
}

void fileManager::reId()
{
    for(int i(0); i < files.size(); i++)
    {
        files[i]->id = i;
    }
}

std::fstream& fileManager::GotoLine(std::fstream& file, unsigned int num)
{
    file.seekg(std::ios::beg);
    for(int i(0); i < num - 1; i++)
    {
        file.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
    }

    return file;
}